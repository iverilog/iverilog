#!/usr/bin/perl -w
use strict;

# list all testcase
my @tc_list=glob '../tc/*';

# run testcase
foreach (@tc_list) {
    s#\.\./tc/##g;
    system "make tc=$_ modelsim";
    #system "mv wave.vcd $_.vcd";
}

####################################################

# generate report
my @log_list=glob '*.log';
my  $report;
open REPORT,'>',"report.txt";

foreach my $log (@log_list) 
{
    open LOG,'<',$log;
  
    my $tc;
    my $result='Fail';
    # Fetch testcase name
    if($log =~ /(\w+)\.log/) {$tc=$1}
  
    while(<LOG>)
    {
        # find report
        if(/done,result:\s+(\w+)/) { $result = $1; }
    }

    $report .= "$tc  $result\n";
}

print   REPORT "testcase    result\n";
print   REPORT $report;

