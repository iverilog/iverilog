
//===============================================
//  Filename       : counter_tb
//  project        : 
//  Department     : 
//  Description    : 
//
//  Author         : wuzhangquan
//  Email          : wzqim@qq.com
//  CreateDate     : 12/12/2018
//  Reversion      : 
//  Release Info   : 
//==============================================

`timescale  1ns/1ns
module  counter(
    input   clk,
    input   rst_n,
    output  reg [7:0]   cnt);

always @(posedge clk or negedge rst_n)
    if(!rst_n)  cnt <= 0;
    else    cnt <= cnt + 1;

endmodule // counter

