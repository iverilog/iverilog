
//===============================================
//  Filename       : counter_tb
//  project        : 
//  Department     : 
//  Description    :  
//
//  Author         : wuzhangquan
//  Email          : wzqim@qq.com
//  CreateDate     : 12/12/2018 
//  Reversion      : 
//  Release Info   : 
//==============================================
    
`timescale  1ns/1ns
module  counter_tb();
reg                 clk             ;
reg                 rst_n           ;
wire    [7:0]       cnt             ;


counter u_counter(
   .clk             (clk             ),
   .rst_n           (rst_n           ),
   .cnt             (cnt             ) 
);


initial begin
    clk = 0;
    rst_n = 0;
    #100;
    rst_n = 1;
    forever # 10 clk = !clk;
end

testcase    tc();


task    test_done(input n_errors);
    if(n_errors!=0) begin
        $display("%m,Running done,result:  Fail");
    end
    else begin
        $display("%m,Running done,result:  Pass");
    end
    $finish;
endtask







`ifdef DUMP_VCD
initial begin
   $dumpfile("wave.vcd");
   $dumpvars(0,counter_tb);
end
`endif

`ifdef DUMP_FSDB
initial begin
   $fsdbDumpfile("wave.fsdb");
   $fsdbDumpvars(0,counter_tb);
end
`endif

`ifdef DUMP_SHM
initial begin               
   $shm_open("wave.shm");  
   $shm_probe(counter_tb,"AS");    
end                         
`endif


endmodule
