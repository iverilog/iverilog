
# centos6.x 安装iverilog步骤

从 github clone 项目到本地
```
git clone https://github.com/steveicarus/iverilog.git
```
安装依赖
```
sudo yum install gperf
```
编译和安装
```
./configure
  make
sudo make install
```

# 安装gtkwave
下载：http://gtkwave.sourceforge.net/  
安装依赖
```
sudo yum install xz-devel
```
安装
```
./configure 
make
sudo make install

```